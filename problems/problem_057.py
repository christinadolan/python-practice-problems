# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(number):

    total = 0

    for numerator in range(1, number +1):
        denominator = numerator + 1
        quotient = numerator / denominator
        total = total + quotient

    return total

print(sum_fraction_sequence(5))
