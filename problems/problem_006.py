# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

age = 18
mary = False
jim = True
def can_skydive(age, has_consent_form):
#Function that determines if someone can go skydiving based on these criteria
    if age >= 18 or has_consent_form:
        return "You can go skydiving"
    else:
        return "You can't go skydiving"

#the p
# erson must be greater or equal to 18 years old, or
#the person must have a signed consent form
print(can_skydive(age, jim))
print(can_skydive(age, mary))
